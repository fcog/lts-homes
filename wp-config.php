<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ltshomes');

/** MySQL database username */
define('DB_USER', 'ltsdbuser');

/** MySQL database password */
define('DB_PASSWORD', 'Ks7%dsj5sus&$');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'r;8HYD{6)|Y[k#ZGcpa/pU]@eR#PVTL4@@k5kT}1pN=+,+k5Rx;JAP|=-}u[k-ia');
define('SECURE_AUTH_KEY',  ',`+.#p+?yD0+YUjQN}2yq!~X}Ph;hMJ`HMAO~8GD?-&Cdj58h0)q:eFGz^c;5SpZ');
define('LOGGED_IN_KEY',    '6hK`.1:ngqvjL=fRR1g5*by 3d1U pAHB-jt!Sek7-`Y{t;Dlb_Aa!}2PBa-sMF-');
define('NONCE_KEY',        'ss8Gv{v9Xzq96D_~|,$qt}6JH7>fR{9abz&( BT([WolQFQKpR~vW}yUGNPn/+Zq');
define('AUTH_SALT',        ' V.Nh|)/Y<%Q.(6,1y/*|H(-Qa_/*&^*KlqA}x;[u! P:?NW#MeNRa_<{>_TmC.T');
define('SECURE_AUTH_SALT', '?.B;d-=c>bB6}r`mqZ+(q+b2EPv,Xj-pED%GPNgO)uTL![W:)$feLrI^1*%KHKr#');
define('LOGGED_IN_SALT',   '-a<;HT+mU--{jw5CJFPFGgISMc[ctK*(65U>G_Xy%N!IH#gG:z`2adC5/@TsU1fG');
define('NONCE_SALT',       '-F1J:!+Wt?Z)^2|aHrOt6B6Spi#Qskx])|+noeX$S(Ji1S1:(8ySymOx~nYc>n63');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
