<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div class="content">
		<h1>WE ARE <span>SORRY!</span></h1>
		<p>Page not found. <a href='<?php echo home_url() ?>' title='Go back'>Go back</a></p>
	</div><!-- #primary -->

<?php get_footer(); ?>