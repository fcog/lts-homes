<!DOCTYPE html>
<!-- HTML5 Mobile Boilerplate -->
<!--[if IEMobile 7]><html class="no-js iem7"><![endif]-->
<!--[if (gt IEMobile 7)|!(IEMobile)]><!--><html class="no-js" lang="en"><!--<![endif]-->

<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>

    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <?php if (is_home()): ?>
       <title><?php echo get_bloginfo("name"); ?></title>
    <?php elseif (is_category()): ?>
        <title><?php single_cat_title(''); ?></title>
    <?php else: ?>
        <title><?php the_title(); ?></title>
    <?php endif ?>

    <meta http-equiv="cleartype" content="on">

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">

    <!-- Responsive and mobile friendly stuff -->
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.8.2.min.js"></script>


    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" media="all">


<?php wp_head(); ?>
</head>

<body>

<div id="wrapper">
    <div id="inner-headcontainer">
        <header class="inner-header">
            <div class="back">
                <a href="<?php echo home_url() ?>"></a>
            </div>
            <figure id="logo">
                <a href="<?php echo home_url() ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-tls-home.png" /></a>
            </figure>
            <div id="options-bar">
                <div id="social-button">
                    <ul class="social-options">
                    <?php $options = get_option( 'APF_AddForms' ); ?>
                    <?php $social_media_links = $options['theme_settings']["theme_settings_form"]["social_media_form"]; ?>
                    <?php if (!empty($social_media_links[0])): ?><li class='facebook'><a href='http://www.facebook.com/<?php echo $social_media_links[0] ?>' target='_blank'></a></li><?php endif ?>
                    <?php if (!empty($social_media_links[1])): ?><li class='twitter'><a href='http://www.twitter.com/<?php echo $social_media_links[1] ?>' target='_blank'></a></li><?php endif ?>
                        <li class="rss"><a href="rss"></a></li>
                    </ul>
                </div>
            </div>
        </header>
    </div>
<div id="maincontentcontainer">
    <section id="inner-page">
        <div class="maincontent">