<?php
/*
Template Name: Pokonos
*/

get_header(); 
?>

<?php if ( have_posts() ) : ?>
	<div class="content">
	    <h1><?php echo stylize_title(the_title("","",false)); ?></h1>
	    <?php while ( have_posts() ) : the_post(); ?>
	        <?php the_content() ?>
	    <?php endwhile; ?>
	</div>
<?php endif; ?>

<?php 
$args = array('post_type' => array('pokono_slide'));  
$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ):
?>	
	<div id="slider-wrapper" class="section group">
		<div id="slideshow">
			<ul class="slides">
		        <?php  while ( $the_query->have_posts() ) : $the_query->the_post(); ?>		
					<li>
						<div class="image">
							<?php if (has_post_thumbnail()) the_post_thumbnail('full'); ?>
						</div>
						<div class="text">
							<div class="content-block">
								<h2><?php the_title(); ?></h2>
								<p><?php the_content(); ?></p>
							</div>
						</div>
					</li>
				<?php endwhile ?>
			</ul>
		</div>
	</div>
<?php endif ?>

<?php get_footer(); ?> 