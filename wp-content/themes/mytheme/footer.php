        </div>
    </section>
</div>    
    <div id="footercontainer">
        <footer class="group">
            <div class="black-half span_6_of_12_complete">
                <div class="finder">
                    <h3>WE can help you find<br> your best choice</h3>
                    <div class="button-finder"><a href="best-choice" class="view">Let&#8217;s start</a></div> 
                </div>
            </div>
            <div class="gray-half span_6_of_12_complete">
                <div class="contact-info">
                    <?php $options = get_option( 'APF_AddForms' ); ?>
                    <?php $contact_info = $options['theme_settings']["theme_settings_form"]["contact_form"]; ?>
                    <?php if (!empty($contact_info[0])): ?>
                        <div class="e-mail">
                            <a href="mailto:<?php echo $contact_info[0] ?>"><?php echo $contact_info[0] ?></a>
                        </div>
                    <?php endif ?>
                    <?php if (!empty($contact_info[1])): ?>
                        <div class="phone"><?php echo $contact_info[1] ?></div>
                    <?php endif ?>
                    <?php if (!empty($contact_info[2])): ?>
                        <div class="address"><?php echo $contact_info[2] ?></div>
                    <?php endif ?>
                </div>
            </div>
            <div class="section group">
                <div class="copyright">
                    <p><a href="#">Legal</a> © 2014 LTS HOMES</p>
                </div>
            </div>
        </footer>
    </div>
        </footer>
    </div>
</div>
<!--jQuery-->

    <!-- Avoid write Compatibility prefixes -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/prefixfree.min.js"></script>

    <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements and feature detects -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.5.3-min.js"></script>

    <!--Only Mac - Safari Class-->
    <script type="text/javascript">
    jQuery(function(){
        // console.log(navigator.userAgent);
        /* Adjustments for Safari on Mac */
        if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Mac') != -1) {
            jQuery('html').addClass('mac'); // provide a class for the safari-mac specific css to filter with
        }
    });
    </script>

    <!--Selectivizr-->
    <!--[if (gte IE 6)&(lte IE 8)]>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/selectivizr.js"></script>
    <![endif]-->

    <!--[if IE]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if gte IE 9]>
        <style type="text/css">
         .gradient {
            filter: none;
        }
     </style>
    <![endif]-->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/venobox/venobox.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.stellar.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/adjustTextSize.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/detect.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/general.js"></script>
<?php wp_footer() ?>
</body>
</html>