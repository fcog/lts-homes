<div class="content">
	<div class="houses">

        <h1><?php echo stylize_title(the_title("","",false)); ?></h1>

        <?php if (has_post_thumbnail()): ?>
            <div class="project-image">
                <?php the_post_thumbnail('homepage-project'); ?>
            </div>
        <?php endif ?>

        <div class="project-content">
            <?php the_content() ?>
            <div>
            	<span>Bedrooms:</span>
            	<?php the_field('bedrooms') ?>
            </div>
            <div>
            	<span>Bathrooms:</span>
            	<?php the_field('bathrooms') ?>
            </div>
            <div>
            	<span>First Floor:</span>
            	<?php if (get_post_meta( get_the_ID(), 'first_floor_suite', true ) == 1) echo "yes"; else echo "no"; ?>
            </div>
            <?php if (get_field('price')): ?> 
            <div>
            	<span>Price:</span>
            	<?php the_field('price') ?>
            </div>
            <?php endif ?>
            <?php if (get_field('floor_plans')): ?>
                <div>
                    <span>Download Floor Plans:</span>
                    <a href="<?php the_field('floor_plans') ?>" title="Download Floor Plans">Download Floor Plans</a>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>