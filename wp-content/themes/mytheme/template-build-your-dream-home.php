<?php
/*
Template Name: Build your dream home
*/

get_header(); 
?>
<div class="content">
	<h1><?php echo stylize_title(the_title("","",false)); ?></h1>

<?php
if (isset($_POST)){

	$meta_query = array('relation' => 'AND');

	if (isset($_POST['bedrooms']) && !empty($_POST['bedrooms'])){
		$bedrooms = trim($_POST['bedrooms']);
		$meta_query[] = array(
	        'key' => 'bedrooms',
	        'value' => $bedrooms, // This is OK, WP_Query will sanitize input!
	        'type' => 'numeric',
	        'compare' => '=',
	    );
	}

	if (isset($_POST['bathrooms']) && !empty($_POST['bathrooms'])){
		$bathrooms = trim($_POST['bathrooms']);
		$meta_query[] = array(
	        'key' => 'bathrooms',
	        'value' => $bathrooms, // This is OK, WP_Query will sanitize input!
	        'type' => 'numeric',
	        'compare' => '=',
	    );		
	}

	if (isset($_POST['check-1'])){
		$first_floor_suite = ($_POST['check-1'] == 1) ? 1 : 0;
		$meta_query[] =  array(
	        'key' => 'first_floor_suite',
	        'value' => $first_floor_suite, // This is OK, WP_Query will sanitize input!
	        'type' => 'numeric',
	        'compare' => '=',
	    );
	}

	
	$price = $_POST['price'];

	if ($price == 1){
		$meta_query[] = array(
	        'key' => 'price',
	        'value' => '200000', // This is OK, WP_Query will sanitize input!
	        'type' => 'numeric',
	        'compare' => '>',
	    );
	    $meta_query[] = array(
	        'key' => 'price',
	        'value' => '250000', // This is OK, WP_Query will sanitize input!
	        'type' => 'numeric',
	        'compare' => '<',
	    );
	}
	elseif ($price == 2){
		$meta_query[] = array(
	        'key' => 'price',
	        'value' => '250000', // This is OK, WP_Query will sanitize input!
	        'type' => 'numeric',
	        'compare' => '>',
	    );
	    $meta_query[] = array(
	        'key' => 'price',
	        'value' => '300000', // This is OK, WP_Query will sanitize input!
	        'type' => 'numeric',
	        'compare' => '<',
	    );
	}
	else{
		$meta_query[] = array(
	        'key' => 'price',
	        'value' => '300000', // This is OK, WP_Query will sanitize input!
	        'type' => 'numeric',
	        'compare' => '>',
	    );
	    $meta_query[] = array(
	        'key' => 'price',
	        'value' => '350000', // This is OK, WP_Query will sanitize input!
	        'type' => 'numeric',
	        'compare' => '<',
	    );
	}

	$args = array(
	    'post_type' => 'project',
	    'posts_per_page' => -1, // -1 to display all results at once
	    'order' => 'ASC',
	    'meta_query' => $meta_query,
	);

	$query = new WP_Query($args);

	// echo $query->request;

    if ($query->have_posts()) : 
    ?>
	<table>
		<caption>Search Results</caption>
		<thead>
			<tr>
				<th></th>
				<th>Design</th>
				<th>Bedrooms</th>
				<th>Bathrooms</th>
				<th>First Floor</th>
				<th>Garages</th>
				<th>Stories</th>
				<th>Homes Starting from</th>
			</tr>
		</thead>
		<tbody>
			<?php while ($query->have_posts()) : $query->the_post(); ?>
		    	<tr>
			        <td>
			        	<a href="<?php the_permalink() ?>" title=""><?php if (has_post_thumbnail()) the_post_thumbnail(); else the_title(); ?></a>
			        </td>
			        <td>
			        	<?php the_title(); ?>
			        </td>			        
			        <td>
			        	<?php the_field('bedrooms') ?>
			        </td>
			        <td>
			        	<?php the_field('bathrooms') ?>
			        </td>
			        <td>
			        	<?php if (get_post_meta( get_the_ID(), 'first_floor_suite', true ) == 1) echo "yes"; else echo "no"; ?>
			        </td>	  
			        <td>
			        	<?php the_field('garage') ?>
			        </td>
			        <td>
			        	<?php the_field('stories') ?>
			        </td>	
			        <td>
			        	<?php echo "$" . number_format(get_field('price')) . ".00" ?>
			        </div>		        	                          
			    </tr>
            <?php endwhile; ?>
        </tbody>
	</table>

	<?php
    else:
    	echo "No results were found. Please try a new search. <a href='".home_url()."' title='Go back'>Go back</a>";
    endif;
}
else{
	echo "No results were found. Please try a new search. <a href='".home_url()."' title='Go back'>Go back</a>";
}

?>

<?php get_footer(); ?> 