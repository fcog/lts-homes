<?php
/*
Template Name: Home
*/
?>
<!DOCTYPE html>
<!-- HTML5 Mobile Boilerplate -->
<!--[if IEMobile 7]><html class="no-js iem7"><![endif]-->
<!--[if (gt IEMobile 7)|!(IEMobile)]><!--><html class="no-js" lang="en"><!--<![endif]-->

<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>

    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <?php if (is_home()): ?>
       <title><?php echo get_bloginfo("name"); ?></title>
    <?php elseif (is_category()): ?>
        <title><?php single_cat_title(''); ?></title>
    <?php else: ?>
        <title><?php the_title(); ?></title>
    <?php endif ?>

    <meta http-equiv="cleartype" content="on">

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">

    <!-- Responsive and mobile friendly stuff -->
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/preloader.js"></script>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" media="all">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/venobox/venobox.css" media="all">

</head>

<body>

<div id="wrapper">
    <div id="headcontainer">
        <header>
            <figure id="logo">
                <a href="#home"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-tls-home.png" /></a>
            </figure>
            <div id="options-bar">
                <div id="social-button">
                    <ul class="social-options">
                    <?php $options = get_option( 'APF_AddForms' ); ?>
                    <?php $social_media_links = $options['theme_settings']["theme_settings_form"]["social_media_form"]; ?>
                    <?php if (!empty($social_media_links[0])): ?><li class='facebook'><a href='http://www.facebook.com/<?php echo $social_media_links[0] ?>' target='_blank'></a></li><?php endif ?>
                    <?php if (!empty($social_media_links[1])): ?><li class='twitter'><a href='http://www.twitter.com/<?php echo $social_media_links[1] ?>' target='_blank'></a></li><?php endif ?>
                        <li class="rss"><a href="rss"></a></li>
                    </ul>
                </div>
                <div id="menu-button">
                    <p>Menu</p> 
                </div>
                <nav class"main-menu">
                    <ul>
                        <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                    </ul>
                </nav>
            </div>
        </header>
    </div>
    <div id="maincontentcontainer">
        <section id="home" class="section" data-stellar-background-ratio="0.5" data-stellar-horizontal-offset="50">
            <div class="mask"></div>
            <div class="maincontent">
                <div id="box-form-home">
                    <div class="copy"><h2><span>The</span><br> Good<br> Life<br> <span>Awaits</span></h2></div>
                    <div id="form-home-designs">
                        <div class="title"><h3>Build Your Dream Home Today in the Poconos, PA!</h3></div>
                        <form action="build-your-dream-home" method="post" id="build">
                            <label>Bedrooms</label>
                            <input type="number" name="bedrooms" id="bedrooms" min="3" max="5">
                            <label>Bathrooms</label>
                            <input type="number" name="bathrooms" id="bathrooms" min="2" max="3">
                            <hr>
                            <label>First Floor Suite</label>
                            <div class="checkbox-1">
                                <input type="checkbox" id="check-1" value="1" name="check" />
                                <label for="check-1"></label>
                            </div>
                            Yes
                            <div class="checkbox-1">
                                <input type="checkbox" id="check-2" value="0" name="check" />
                                <label for="check-2"></label>
                            </div>
                            No
                            <hr>
                            <label>Price range </label>
                            <select name="price">
                              <option value="1">200.000-250.000</option>
                              <option value="2">251.000-300.000</option>
                              <option value="3">301.000-350.000</option>
                            </select>
                            <input type="submit" value="Show available home designs">
                        </form>
                    </div>
                </div>
                <div class="next-button"><a href="#video"></a></div>
            </div>
        </section>
        <section id="video" class="section">
            <div class="maincontent">
                <div class="section group">
                    <div id="player-image" class="span_12_of_12">
                        <?php $video_tour_url = $options['theme_settings']["theme_settings_form"]["video_tour_form"]; ?>
                        <a href="<?php echo $video_tour_url[0] ?>" class="player-button venobox" data-type="iframe"></a>
                    </div>
                    <div id="video-title">
                        <h2>Video Tour</h2>
                        <div class="subtitle-red">Exceeding</div>
                        <div class="subtitle-black">Expectations</div>
                    </div>
                </div>
            </div>
        </section>
        <section id="newsletter" class="section">
            <div data-stellar-background-ratio="0.5" class="maincontent">
                <div class="content section group">
                    <p class="col span_7_of_12">Sign Up to Our Newsletter 
                    for Exclusive Offers</p>
              <!--       <form class="email-updates col span_5_of_12">
                        <input type="text" name="fmail" placeholder="Write your Email">
                        <input type="submit" value="submit">
                    </form> -->
                    <?php echo do_shortcode('[constantcontactapi formid="1"]'); ?>
                </div>
            </div>
        </section>
        <?php 
        $args = array('post_type' => array('project'));  
        $the_query = new WP_Query( $args );

        $total_posts = $the_query->post_count;

        if ( $the_query->have_posts() ):
        ?>
            <section id="home-designs" class="section">
                <div class="maincontent">
                    <div class="set_of_projects section group">
                        <div id="slideshow-projects">
                            <ul class="slides">                
                            <?php 
                            $i=0;
                            while ( $the_query->have_posts() ) : $the_query->the_post();
                                if ($i%8 == 0): 
                            ?>
                                    <li>
                                <?php endif ?>
                                    <div class="design-project">
                                        <div class="project-info">
                                            <h3><?php the_title(); ?></h3>
                                            <?php the_excerpt(); ?>
                                            <div class="button-view"><a href="<?php the_permalink() ?>" class="view">View</a></div>
                                        </div>
                                        <?php if (has_post_thumbnail()) the_post_thumbnail('homepage-project'); ?>
                                    </div>
                                <?php $i++; ?>
                                <?php if ($i%8 == 0 || $i==$total_posts):  ?>
                                    </li>
                                <?php endif ?>
                            <?php endwhile ?>
                            </ul>
                        </div>
                        <a href="#" class="prev"></a>
                        <a href="#" class="next"></a>
                    </div>
                </div>
            </section>
        <?php endif ?>
        <?php
        $page = get_page_by_title( 'Pocono Lifestyle' );
        if( $page ):
        ?>        
            <section id="poconos" class="section">
                <div class="maincontent">
                    <div class="section group">
                        <h2>Pocono <span>Lifestyle</span></h2>
                    </div>
                    <div class="section group">
                        <img src="<?php echo get_template_directory_uri(); ?>/photos/poconos-img-intro.png">
                        <p><?php echo $page->post_excerpt ?></p>
                        <a href="pocono-lifestyle" class="view">view</a>
                    </div>
                </div>
            </section>
        <?php endif ?>
        <?php 
        $args = array('post_per_page' => 6);  
        $the_query = new WP_Query( $args );

        $total_posts = $the_query->post_count;

        if ( $the_query->have_posts() ):
        ?>        
            <section id="media" class="section">
                <div class="maincontent">
                    <div id="slideshow-media">
                        <ul class="slides">
                            <?php 
                                $i=0;
                                while ( $the_query->have_posts() ) : $the_query->the_post();
                                    if ($i%3 == 0): 
                            ?>
                                    <li>
                                <?php endif ?>
                                        <article class="col span_1_of_3">
                                            <p class="date">/ <time><?php the_time('F j, Y') ?></time></p> 
                                            <h3><!--<a href="<?php the_permalink() ?>">--><?php the_title(); ?><!--</a>--></h3>
                                            <?php the_excerpt() ?>
                                        </article>
                                <?php $i++; ?>
                                <?php if ($i%3 == 0 || $i==$total_posts):  ?>
                                    </li>
                                <?php endif ?>
                            <?php endwhile ?>
                        </ul>
                    </div>
                </div>
            </section>
        <?php endif ?>
        <section id="about" class="section">
            <div class="maincontent">
                <div class="section group">
                    <div class="col span_1_of_2">
                        <div class="topic">
                            <div class="icon history">
                            </div>
                            <?php
                            $page = get_page_by_title( 'Our history' );
                            if( $page ):
                            ?>
                                <h3><a href="our-history">Our history</a></h3>
                                <p><?php echo $page->post_excerpt ?></p>
                                <a href="our-history" class="readmore"><span>Read More</span></a>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="col span_1_of_2">
                        <div class="topic">
                            <div class="icon promise">
                            </div>
                            <?php
                            $page = get_page_by_title( 'Our Quality Promise' );
                            if( $page ):
                            ?>
                                <h3><a href="our-quality-promise">Our Quality Promise</a></h3>
                                <p><?php echo $page->post_excerpt ?></p>
                                <a href="our-quality-promise" class="readmore"><span>Read More</span></a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <div class="section group">
                    <div class="col span_1_of_2">
                        <div class="topic">
                            <div class="icon studio">
                            </div>
                            <?php
                            $page = get_page_by_title( 'LTS Homes Design Studio' );
                            if( $page ):
                            ?>                            
                                <h3><a href="lts-homes-design-studio">LTS Homes Design Studio</a></h3>
                                <p><?php echo $page->post_excerpt ?></p>
                                <a href="lts-homes-design-studio" class="readmore"><span>Read More</span></a>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="col span_1_of_2">
                        <div class="topic">
                            <div class="icon financing">
                            </div>
                            <?php
                            $page = get_page_by_title( 'Financing Options' );
                            if( $page ):
                            ?>                             
                                <h3><a href="financing-options">Financing Options</a></h3>
                                <p><?php echo $page->post_excerpt ?></p>
                                <a href="financing-options" class="readmore"><span>Read More</span></a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="footercontainer">
        <footer class="group">
            <div class="black-half span_6_of_12_complete">
                <div class="finder">
                    <h3>WE can help you find<br> your best choice</h3>
                    <div class="button-finder"><a href="best-choice" class="view">Let&#8217;s start</a></div> 
                </div>
            </div>
            <div class="gray-half span_6_of_12_complete">
                <div class="contact-info">
                    <?php $contact_info = $options['theme_settings']["theme_settings_form"]["contact_form"]; ?>
                    <?php if (!empty($contact_info[0])): ?>
                        <div class="e-mail">
                            <a href="mailto:<?php echo $contact_info[0] ?>"><?php echo $contact_info[0] ?></a>
                        </div>
                    <?php endif ?>
                    <?php if (!empty($contact_info[1])): ?>
                        <div class="phone"><?php echo $contact_info[1] ?></div>
                    <?php endif ?>
                    <?php if (!empty($contact_info[2])): ?>
                        <div class="address"><?php echo $contact_info[2] ?></div>
                    <?php endif ?>
                </div>
            </div>
            <div class="section group">
                <div class="copyright">
                    <p><a href="#">Legal</a> © 2014 LTS HOMES</p>
                </div>
            </div>
        </footer>
    </div>
</div>

<!--jQuery-->

    <!-- Avoid write Compatibility prefixes -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/prefixfree.min.js"></script>

    <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements and feature detects -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.5.3-min.js"></script>

    <!--Only Mac - Safari Class-->
    <script type="text/javascript">
    jQuery(function(){
        // console.log(navigator.userAgent);
        /* Adjustments for Safari on Mac */
        if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Mac') != -1) {
            jQuery('html').addClass('mac'); // provide a class for the safari-mac specific css to filter with
        }
    });
    </script>

    <!--Selectivizr-->
    <!--[if (gte IE 6)&(lte IE 8)]>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/selectivizr.js"></script>
    <![endif]-->

    <!--[if IE]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if gte IE 9]>
        <style type="text/css">
         .gradient {
            filter: none;
        }
     </style>
    <![endif]-->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/venobox/venobox.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.stellar.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/adjustTextSize.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/detect.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/general.js"></script>
</body>
</html>