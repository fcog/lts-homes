<?php

require get_template_directory() . '/admin.php';

function mytheme_setup() {
    register_nav_menu( 'primary', __( 'Navigation Menu', 'mytheme' ) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 150, 150, true );
    add_image_size( 'homepage-project', 475, 475, true ); 
    // add_image_size( 'leadership', 175, 175, true ); 
}
add_action( 'after_setup_theme', 'mytheme_setup' );

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

function constant_contact_function($widget) {
    // The $widget variable is the output of the widget
    // This will replace 'this word' with 'that word' in the widget output.
    $widget = str_replace('class="kws_form "', 'class="kws_form email-updates col span_5_of_12"', $widget);
    // Make sure to return the $widget variable, or it won't work!
    return $widget;
}
add_filter('constant_contact_form', 'constant_contact_function');

add_action( 'init', 'register_cpt_project' );
function register_cpt_project() {
    $labels = array(
    'name' => _x( 'Project', 'project' ),
    'singular_name' => _x( 'Project', 'project' ),
    'add_new' => _x( 'Add New', 'project' ),
    'add_new_item' => _x( 'Add New Project', 'project' ),
    'edit_item' => _x( 'Edit Project', 'project' ),
    'new_item' => _x( 'New Project', 'project' ),
    'view_item' => _x( 'View Project', 'project' ),
    'search_items' => _x( 'Search Projects', 'project' ),
    'not_found' => _x( 'No projects found', 'project' ),
    'not_found_in_trash' => _x( 'No projects found in Trash', 'project' ),
    'parent_item_colon' => _x( 'Parent Project:', 'project' ),
    'menu_name' => _x( 'Projects', 'project' ),
    );
    $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'description' => 'Homepage project posts',
    'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 20,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
    'capability_type' => 'post'
    );
    register_post_type( 'project', $args );
} 

add_action( 'init', 'register_cpt_pokono_slide' );
function register_cpt_pokono_slide() {
    $labels = array(
    'name' => _x( 'Pokono Slide', 'pokono_slide' ),
    'singular_name' => _x( 'Pokono Slide', 'pokono_slide' ),
    'add_new' => _x( 'Add New', 'pokono_slide' ),
    'add_new_item' => _x( 'Add New Pokono Slide', 'pokono_slide' ),
    'edit_item' => _x( 'Edit Pokono Slide', 'pokono_slide' ),
    'new_item' => _x( 'New Pokono Slide', 'pokono_slide' ),
    'view_item' => _x( 'View Pokono Slide', 'pokono_slide' ),
    'search_items' => _x( 'Search Pokono Slides', 'pokono_slide' ),
    'not_found' => _x( 'No Pokono Slides found', 'pokono_slide' ),
    'not_found_in_trash' => _x( 'No Pokono Slides found in Trash', 'pokono_slide' ),
    'parent_item_colon' => _x( 'Parent Pokono Slide:', 'pokono_slide' ),
    'menu_name' => _x( 'Pokono Slides', 'pokono_slide' ),
    );
    $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'description' => 'Homepage pokono slide posts',
    'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 20,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
    'capability_type' => 'post'
    );
    register_post_type( 'pokono_slide', $args );
} 

function stylize_title($text){
    $title_elements = explode(" ", $text);

    $middle = ceil(count($title_elements) / 2) + 1;

    $stylized_title = "";

    for ($i=1; $i <= count($title_elements); $i++) { 
        if ($i == $middle) {
            $stylized_title .= "<span>";
        }
        $stylized_title .= $title_elements[$i-1] . " ";
    }

    $stylized_title .= "</span>";

    return $stylized_title;
}