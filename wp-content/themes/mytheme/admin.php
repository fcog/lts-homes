<?php
/************* ADMIN PAGE *****************/

// Include the library class
if ( !class_exists( 'AdminPageFramework' ) )
    include_once( dirname( __FILE__ ) . '/class/admin-page-framework.php' );
 
// extend the class
class APF_AddForms extends AdminPageFramework {
 
    // Define the setup method to set how many pages, page titles and icons etc.
    public function setUp() {
       
        // Create the root menu
        $this->setRootMenuPage( 'Settings' );        // specifies to which parent menu to add.
        // Dashboard, Posts, Media, Links, Pages, Comments, Appearance, Plugins, Users, Tools, Settings, Network Admin
       
        // Add the sub menus and the pages
        $this->addSubMenuPage(   
            'Theme Settings',        // the page and menu title
            'theme_settings'         // the page slug
        );    
 
        // Add form sections.
        $this->addSettingSections(
            array(
                'strSectionID'    => 'theme_settings_form',    // the section ID
                'strPageSlug'    => 'theme_settings',    // the page slug that the section belongs to
                'strTitle'    => 'Homepage Settings',    // the section title
            )
        );       
        $this->addSettingFields(
            array(  // Multiple text fields
              'strFieldID' => 'social_media_form',
              'strSectionID' => 'theme_settings_form',
              'strTitle' => __( 'Social Media Links', 'my-theme' ),
              'strType' => 'text',
              'vLabel' => array( 
                'Facebook: ', 
                'Twitter: ', 
              ),
              'vSize' => array(
                40,
                40,
              ),
              'vDelimiter' => '<br />',
            ),
            array(  // Multiple text fields
              'strFieldID' => 'contact_form',
              'strSectionID' => 'theme_settings_form',
              'strTitle' => __( 'Contact Information', 'my-theme' ),
              'strType' => 'text',
              'vLabel' => array( 
                'E-mail: ', 
                'Phone: ', 
                'Address: ', 
              ),
              'vSize' => array(
                40,
                40,
                40,
              ),
              'vDelimiter' => '<br />',
            ),
            array(  // Multiple text fields
              'strFieldID' => 'video_tour_form',
              'strSectionID' => 'theme_settings_form',
              'strTitle' => __( 'Video Tour URL', 'my-theme' ),
              'strType' => 'text',
              'vLabel' => array( 
                'Video Youtube URL: ', 
              ),
              'vSize' => array(
                100,
              ),
              'vDelimiter' => '<br />',
            ),    
            array( // Submit button
                'strFieldID' => 'submit_button',
                'strSectionID' => 'theme_settings_form',
                'strType' => 'submit',
                'vLabel' => __( 'Save Changes' ),
            )
        );                 
    }

   
}
 
// Instantiate the class object.
if ( is_admin() )
    new APF_AddForms;