$(window).ready(function() {
    var wi = $(window).width();  
 
    $(window).resize(function() {
        var wi = $(window).width();
 
        if (wi <= 1265){
        	$("nav").css("display","none");
            }
        else {
            $("nav").css("display","block");
            }
    }); 
});

$(document).ready(function(){

  //FONT RESIZE

  $(".subtitle-red, .subtitle-black").adjustTextSize();

  //PARALLAX

  jQuery(window).stellar();

  // VIDEO OVERLAY
  jQuery("a.player-button").venobox(); 

  //MENU

  $(window).on("scroll", function () {
    if ($(this).scrollTop() > 850) {
        $("header").addClass("grayback");
    }
    else {
        $("header").removeClass("grayback");
    }
  });

  $("#menu-button").click(function () {
    $(".social-options").each(function() {
         displaying = $(this).css("display");
          if(displaying == "block" ) {
             $(this).animate({
                width: 'toggle'
              },'fast', "linear", function() {
                $(this).css("display","none");
             });
          }
    });
     $("nav").each(function() {
        displaying = $(this).css("display");
          if(displaying == "block" ) {
             $(this).animate({
                width: 'toggle'
              },'fast', "linear", function() {
                $(this).css("display","none");
             });
          } else {
             $(this).animate({
                width: 'toggle'
              },'fast', "linear", function() {
                $(this).css("display","block");
             });
          }
      });
  });

  //SOCIAL NETWORK

  $("#social-button").click(function () {
    $("nav").each(function() {
        displaying = $(this).css("display");
        width = $(window).width();
          if((width <= 1280) && (displaying == "block")) {
             $(this).animate({
                width: 'toggle'
              },'fast', "linear", function() {
                $(this).css("display","none");
             });
          }
      });
     $(".social-options").each(function() {
         displaying = $(this).css("display");
          if(displaying == "block" ) {
             $(this).animate({
                width: 'toggle'
              },'fast', "linear", function() {
                $(this).css("display","none");
             });
          } else {
             $(this).animate({
                width: 'toggle'
              },'fast', "linear", function() {
                $(this).css("display","block");
             });
          }
      });
  });
	
  //ANIMATES ANCHOR

  $('a[href*=#]').click(function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
    	&& location.hostname == this.hostname) {
            var $target = $(this.hash);
            $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
            if ($target.length) {
                var targetOffset = $target.offset().top;
                $('html,body').animate({scrollTop: targetOffset}, 700);
                return false;
        	}
    	}
	});

  // SLIDER

  jQuery('#slideshow, #slideshow-media').flexslider({
      animation: "fade",
      direction: "horizontal",
      slideshowSpeed: 7000,
      animationSpeed: 600,
      directionNav: false,
      controlNav:true,
      initDelay: 0,
      prevText: "",           
      nextText: "",
      pauseOnHover: true,
  });

  jQuery('#slideshow-projects').flexslider({
      animation: "slide",
      direction: "horizontal",
      slideshowSpeed: 7000,
      animationSpeed: 600, 
      slideshow: false,
      directionNav: true,
      controlNav: false,
      initDelay: 0,
      prevText: "",           
      nextText: "",
      pauseOnHover: true,
  });

  // HOME DESIGNS
  
  $('.design-project').mouseenter(function() {
    var gallery = $(this);
    gallery.find('.project-info').stop().fadeIn(100);
    gallery.find('img').stop().animate({
      opacity: 0.05
    }, 300, function () {});
    
  })
  .mouseleave(function(){
    var gallery = $(this);
    gallery.find('.project-info').stop().fadeOut(0);
    gallery.find('img').stop().animate({
      opacity: 1.00
    }, 300, function () {});
  });

  //BUILD FORM CHECKBOXES
  $("#build input:checkbox").click(function() {
      if ($(this).is(":checked")) {
          var group = "input:checkbox[name='" + $(this).attr("name") + "']";
          $(group).prop("checked", false);
          $(this).prop("checked", true);
      } else {
          $(this).prop("checked", false);
      }
  });  

});
