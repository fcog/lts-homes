<html>
<head>
	<link rel="stylesheet" type="text/css" href="styles.css" />
</head>
<body>
<h1>Migration Script for WP</h1>
<?php
/**
 * Script for updating WP instance when migrating among domains or paths
 * @author felipe@inqbation.com
 * @copyright Lufeceba 2008
 * @version 1.2
 */

	/** check if wp-config file present **/
	$form_msg = "";
	if (file_exists("../wp-config.php")) {
		include("../wp-config.php");
		$conn['dbname'] = DB_NAME;
		$conn['dbuser'] = DB_USER;
		$conn['dbpwd'] = DB_PASSWORD;
		$conn['server'] = DB_HOST;
		$settings['table_prefix'] = $table_prefix; //taken from file
	} else {
		$form_msg = "File wp-config.php not present, you should configure the DB connection params";
	}

	/** check if form was submitted **/
	if( !empty($_POST) ) {
		extract($_POST);
		if(!empty($conn_dbname)) {
			$conn['dbname'] = $conn_dbname;
			$conn['dbuser'] = $conn_dbuser;
			$conn['dbpwd'] = $conn_password;
			$conn['server'] = $conn_hostname;
			$settings['table_prefix'] = $table_prefix;
		}
	}

//	some general settings
	$settings["olddomain"] = trim($old_domain);
	$settings["newdomain"] = trim($new_domain);
	$settings["checkroot"] = $check_root;  // checks entries without http://localhost/ string, but the remaining string
	$settings["oldpath"] = trim(stripslashes($old_path));
	$settings["newpath"] = trim($new_path)?trim(stripslashes($new_path)):dirname(dirname( __FILE__ ));  // use this file's path or
//	$settings["newpath"] = 'F:\webdevelop\htdocs\clients\sb';  // set it manually
	$settings["debug"] = $debug_mode;  // set to false so that the update queries are really executed



  // Johan Added the next lines for multisite support
  
  echo "connecting to the database server<br/>";
  $link = mysql_connect( $conn["server"], $conn["dbuser"], $conn["dbpwd"] ) or die( "Unable to connect to the DB Engine" );

  echo "Changing the database<br />";
  mysql_select_db( $conn["dbname"], $link ) or die( "Unable to access the database, check the privileges" );
  
  $query = "SELECT blog_id,site_id,domain FROM {$settings['table_prefix']}blogs"; // ... WHERE option_name LIKE '%widget%'
  $res = mysql_query( $query, $link ) or die ( "Unable to get blogs records" );
  $the_sites = array();

  if($res) 
  {
    while ( $row = mysql_fetch_assoc($res) ) 
    {
      array_push($the_sites,$row);
    }
  }
  
  
  // Add information about the corresponding table prefix
  $index = 0;
  foreach($the_sites as $a_site)
  {
    if($a_site['blog_id'] == 1){
      $the_sites[$index]['blog_prefix'] = $settings['table_prefix'];
    }
    else{
      $the_sites[$index]['blog_prefix'] = $settings['table_prefix'].$a_site['blog_id'] .'_' ;
    }
    $index++;
  }
  print_r($the_sites);
  
  //execute the necessary query to update the blogs table
  /*$query = "UPDATE {$settings['table_prefix']}blogs SET domain = replace(domain,'{$settings["olddomain"]}','{$settings["newdomain"]}')";
  echo "executing $query <br />";
    if ( !$settings["debug"] ) {
      mysql_query( $query, $link );
  }*/
    
  


	// load the form
	include("form.php");

	if (!empty($run_queries)) {
?>
	<fieldset>
		<legend>Results</legend>
<?php

	// Let's begin
		if ( $settings["debug"] ) {
			echo "<p style='background-color: #ddd'>Debug is true, did you already configured everything??</p>";
		} else {
			echo "<p style='background-color: #ddd'>Let's run the scripts..... </p>";
		}

		echo "Checking magic_quotes_gpc... ";
		echo ini_get("magic_quotes_gpc");
		echo "<br />";

	  $oldpath = mysql_real_escape_string( $settings["oldpath"], $link );
		$newpath = mysql_real_escape_string( $settings["newpath"], $link );
    
    $oldurl = parse_url( $settings["olddomain"] );
    $newurl = parse_url( $settings["newdomain"] );
		
		// Johan: Igrouped all the queries to include them on the foreach loop
		
		foreach($the_sites as $a_site){
		//  main queries - don't change them
    $queries["options"] = "UPDATE {$a_site['blog_prefix']}options SET option_value = replace(option_value, '{$settings["olddomain"]}', '{$settings["newdomain"]}') WHERE option_name='home' OR option_name = 'siteurl'";
    $queries["p_guid"] = "UPDATE {$a_site['blog_prefix']}posts SET guid = replace(guid, '{$settings["olddomain"]}','{$settings["newdomain"]}')";
    $queries["p_content1"] = "UPDATE {$a_site['blog_prefix']}posts SET post_content = replace(post_content, '{$settings["olddomain"]}', '{$settings["newdomain"]}')";

    //  try to calculate an absolute path without including the scheme and server
    if ( $settings["checkroot"] ) {
      $queries["p_content2"] = "UPDATE {$a_site['blog_prefix']}posts SET post_content = replace(post_content, '{$oldurl["path"]}', '{$newurl["path"]}')";
    }
    
		// query to change some values in postmeta
		$queries["pm_attfile"] = "UPDATE {$a_site['blog_prefix']}postmeta SET meta_value = replace(meta_value, '$oldpath', '$newpath') WHERE meta_key LIKE '_wp_attached_file'";

		foreach ( $queries as $k=>$query ) {
			echo "executing $query <br />";
			if ( !$settings["debug"] ) {
				mysql_query( $query, $link );
			}
		}
		} //foreach end


    foreach($the_sites as $a_site){
		$queries["get_pm_attmeta"] = "SELECT meta_id, meta_value FROM {$a_site['blog_prefix']}postmeta WHERE meta_key LIKE '_wp_attachment_metadata'";
		
		//echo 'THE QUERY:'.$queries["get_pm_attmeta"]. '<br />';
		$res = mysql_query( $queries["get_pm_attmeta"], $link ) or die ( "Unable to get postmeta records" );

		if ( $res ) 
		{
			while ( $row = mysql_fetch_assoc($res) ) {
				$attmeta = unserialize( $row["meta_value"] );
				$attmeta["file"] = str_replace( $settings["oldpath"], $settings["newpath"], $attmeta["file"] );
				$row["meta_value"] = serialize( $attmeta );

				$metavalue = mysql_real_escape_string( $row["meta_value"], $link );
				$query = "UPDATE {$a_site['blog_prefix']}postmeta SET meta_value = '$metavalue' WHERE meta_id = {$row["meta_id"]}";
				echo "executing* $query <br />";
				if ( !$settings["debug"] ) {
					mysql_query( $query, $link );
				}       
			}
		}
    
    }//end foreach
    
    /* Added by Johan */
    
    foreach($the_sites as $a_site){
      $query = "UPDATE {$a_site['blog_prefix']}postmeta SET meta_value = replace(meta_value,'{$settings["olddomain"]}','{$settings["newdomain"]}')";
      echo "executing* $query <br />";
      if ( !$settings["debug"] ) {
        mysql_query( $query, $link );
      }
    }//end foreach
    
    
    foreach($the_sites as $a_site){
    $query = "SELECT option_id,option_value FROM {$a_site['blog_prefix']}options"; // ... WHERE option_name LIKE '%widget%'
    $res = mysql_query( $query, $link ) or die ( "Unable to get postmeta records" );

    if($res) 
    {
      while ( $row = mysql_fetch_assoc($res) ) {
        $optvalue = unserialize( $row["option_value"] );
        //echo '<br />. . . . . . . . . . . . . . . . . . . . . .OLDVAL:'.$row["option_value"].'<br />';
        
        if( $optvalue!== false && is_array($optvalue))
        {
          replaceRecur($optvalue,$settings["olddomain"],$settings["newdomain"]);
          $row["option_value"] = serialize( $optvalue );
        }
        else
        {
          $row["option_value"]  = str_replace( $settings["olddomain"], $settings["newdomain"], $row["option_value"] );
        }
        $optvalue = mysql_real_escape_string( $row["option_value"], $link );
        $query = "UPDATE {$a_site['blog_prefix']}options SET option_value = '$optvalue' WHERE option_id = {$row["option_id"]}";
        echo "<br />executing1 $query <br />";
        if ( !$settings["debug"] ) {
          mysql_query( $query, $link );
        }       
      }// end while
    }
    }//end foreach
		echo "done!";
	}

  function replaceRecur(&$inputArray,$olddomain,$newdomain)
  {
    foreach($inputArray as $key => $element){
        if(is_array($element))
        {
            replaceRecur($inputArray[$key],$olddomain,$newdomain);
        }
        else
        {
            $inputArray[$key] =str_replace($olddomain, $newdomain,$element);
        }
    }
  }

  



?>
	</fieldset>
</body>
</html>
